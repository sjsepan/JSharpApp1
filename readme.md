# readme for JSharpApp1 v0.1

## About

Desktop GUI app prototype, on Windows, in J# / WinForms

![JSharpApp1.png](./JSharpApp1.png?raw=true "Screenshot")

### Notes

I have not used a partial-class implementation because this IDE does not seem to support it.

### VSCode

VSCode was not used for the primary development, which was done in Visual J# 2005 Express Edition. However, documentation and preparation for posting was done in Code.
To nest files associated with forms, use the setting 'Features/Explorer/File Nesting: Patterns':
Item="*.cs", Value="${basename}.resx,${basename}.Designer.cs"

### Issues

~Int32 return on main(): "System.Int32' is not valid in field or method declarations, use 'int' instead"
~int return on main(): "/target is 'winexe' but no suitable main method was found in compilation"
~It is unclear how to tell the compiler that the main entry point is returning an exit-code to the terminal, a well-known behavior.
~When converting between int and string, note that there is no ToString() extension or built-in method on the string type. Instead, use System.Convert.ToString();
~Value types must extend ValueType: <https://web.archive.org/web/20111220004451/http://msdn.microsoft.com/en-US/library/wysdab55(v=VS.80).aspx>
~Defining and using delegates and event-handlers: <https://web.archive.org/web/20111220004451/http://msdn.microsoft.com/en-US/library/wysdab55(v=VS.80).aspx>

### J# on the Web

<https://www.codeproject.com/articles/1440/introduction-to-visual-j-net>
<https://en.wikipedia.org/wiki/Visual_J_Sharp>
<https://web.archive.org/web/20070416013526/http://msdn2.microsoft.com:80/en-us/vjsharp/bb188610.aspx>
<https://handwiki.org/wiki/J_Sharp>
<http://www.functionx.com/jsharp/Lesson01.htm>
<https://github.com/SleepyDeb/JSharp>
<https://www.vjsharp.net/>
<https://quizack.com/skill-assessment/jsharp-2003-test>

### Related

<https://en.wikipedia.org/wiki/IKVM.NET>

### History

0.1:
~initial release

Steve Sepan
ssepanus@yahoo.com
10/6/2022
